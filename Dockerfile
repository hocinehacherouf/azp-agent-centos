FROM centos:7

LABEL org.label-schema.schema-version=1.0 \
    org.label-schema.vcs-url="https://gitlab.com/hhocine/azp-agent-centos" \
    org.label-schema.name=azp-agent-centos \
    org.label-schema.license=MIT

RUN yum-config-manager \
    --add-repo \
    https://packages.microsoft.com/config/rhel/7/prod.repo

RUN yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

COPY ./adoptopenjdk.repo /etc/yum.repos.d/adoptopenjdk.repo

RUN yum update -y

RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
    gcc-c++ make git2u-all unzip epel-release \
    nodejs powershell dotnet-sdk-2.1 dotnet-sdk-2.2 dotnet-sdk-3.0 \
    adoptopenjdk-11-hotspot \
    yum-utils \
    device-mapper-persistent-data \
    lvm2 \
    ansible \
    docker-ce docker-ce-cli docker-compose containerd.io \
    && yum install -y python-pip

RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -
RUN yum install -y nodejs

RUN pip install truffleHog

RUN yum clean all

ENV AZP_URL=
ENV AZP_TOKEN=
ENV AZP_AGENT_NAME=
ENV AZP_POOL=
ENV AZP_WORK=
ENV AZP_AGENTPACKAGE_URL=

WORKDIR /azp

COPY ./start.sh .

RUN chmod +x start.sh

CMD ["./start.sh"]